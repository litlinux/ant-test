import React from 'react'
import {Router as BrowserRouter, Switch } from 'react-router-dom'

import PrivateRoute from './routes/privateRoute'
import PublicRoute from './routes/publicRoute'
import history from './utils/history'

import { Gallery } from './layouts/Gallery'
import { Welcome } from './layouts/Welcome'

const App = () => {
    return (
        <BrowserRouter history={history}>
            <Switch>
                <PrivateRoute exact path={['/','/popular', '/logout', '/upload']} component={Gallery} />
                <PublicRoute exact path={['/sign-in', '/sign-up']} component={Welcome} />
            </Switch>
        </BrowserRouter>
    )
}

export default App
