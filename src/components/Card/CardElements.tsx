import styled from 'styled-components'
import {Card} from "antd";

export const CardStyled = styled(Card)`
  border-radius: 15px;
  box-shadow: 0px 28px 44px -18px rgb(197 197 197);
  overflow: hidden;
`
