import styled from 'styled-components'
import { LazyLoadImage } from 'react-lazy-load-image-component'

export const LazyLoadImageStyled = styled(LazyLoadImage)`
    border-radius: 15px;
    box-shadow: 0px 7px 19px 11px rgb(197 197 197);
`
