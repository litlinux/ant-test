import styled from 'styled-components'

export const LogoDiv = styled.div`
    color: beige;
    @media (max-width: 767px) {
        text-align: center;
        padding-bottom: 15px;
    }
`
