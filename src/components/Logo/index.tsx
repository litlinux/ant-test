import React from 'react'
import { ReactComponent as Logo } from '../../assets/logo.svg'
import { LogoDiv } from './LogoElements'

const LogoIcon = () => {
    return (
        <LogoDiv>
            <Logo />
        </LogoDiv>
    )
}

export default LogoIcon
