import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
import { ToastContainer } from 'react-toastify'

import App from './App'
import getStore from './store'

import 'antd/dist/antd.css'
import './assets/fonts/Acrom/stylesheet.css'
import './assets/style.scss'
import 'react-toastify/dist/ReactToastify.css'
import 'react-lazy-load-image-component/src/effects/blur.css'

const { store, persistor } = getStore()

ReactDOM.render(
    <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
            <App />
            <ToastContainer />
        </PersistGate>
    </Provider>,
    document.getElementById('root')
)
