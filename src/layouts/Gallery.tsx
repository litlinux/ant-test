import { Col, Row, Spin } from 'antd'
import React, { lazy, Suspense, useEffect } from 'react'
import { Route } from 'react-router-dom'
import Logo from '../components/Logo'
import Navbar from '../components/Navbar'
import Logout from '../pages/Logout'
import New from '../pages/New'
import Popular from '../pages/Popular'
import Upload from "../pages/Upload";

export const Gallery = () => {
    return (
        <div style={{ padding: '15px 24px' }}>
            <header style={{ marginBottom: 45 }}>
                <Row align={'bottom'}>
                    <Col xs={24} sm={24} md={6}>
                        <Logo />
                    </Col>
                    <Col xs={24} sm={24} md={18}>
                        <Navbar />
                    </Col>
                </Row>
            </header>
            <Suspense fallback={<Spin size='large' />}>
                <Route exact path='/' component={New} />
                <Route exact path='/popular' component={Popular} />
                <Route exact path='/upload' component={Upload} />
                <Route exact path='/logout' component={Logout} />
            </Suspense>
        </div>
    )
}
