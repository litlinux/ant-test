import React from 'react'
import { Route } from 'react-router-dom'
import { SignIn } from '../pages/SignIn'
import { Col, Row } from 'antd'
import { SignUp } from '../pages/SignUp'

export const Welcome = () => {
    return (
        <Row style={{ height: '100vh', padding: 20 }} align='middle'>
            <Col span={24}>
                <Route exact path='/sign-in' component={SignIn} />
                <Route exact path='/sign-up' component={SignUp} />
            </Col>
        </Row>
    )
}
