import { useEffect } from 'react'
import { useActions } from '../hooks/useActions'

const LogoutPage = () => {
    const { logout } = useActions()
    useEffect(() => {
        logout()
    })
    return null
}

export default LogoutPage
