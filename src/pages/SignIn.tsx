import { Form, Input, InputNumber, Checkbox, SubmitButton, ResetButton, Select, DatePicker } from 'formik-antd'
import React from 'react'
import { Formik } from 'formik'
import { Col, Row, Typography } from 'antd'
import { Link } from 'react-router-dom'
import { getTokens } from '../api/auth'
import * as Yup from 'yup'
import { useActions } from '../hooks/useActions'

const { Title } = Typography

const formItemLayout = {
    labelCol: { xs: { span: 24 }, sm: { span: 5 } },
    wrapperCol: { xs: { span: 24 }, sm: { span: 14 } },
}

const buttonItemLayout = { wrapperCol: { sm: { span: 14, offset: 5 }, xs: { span: 24, offset: 0 } } }

const SignInSchema = Yup.object().shape({
    username: Yup.string().required(),
    password: Yup.string().min(6, 'Min 6 symbols').required(),
})

export const SignIn = () => {
    const { login } = useActions()
    return (
        <>
            <div>
                <Title style={{ textAlign: 'center' }}>Sign In</Title>
            </div>
            <Row justify={'center'}>
                <Col xs={24} md={14} lg={10}>
                    <Formik
                        initialValues={{
                            username: '',
                            password: '',
                        }}
                        validationSchema={SignInSchema}
                        onSubmit={async (values, actions) => {
                            await getTokens(values, login)
                        }}
                        render={() => (
                            <Form {...formItemLayout}>
                                <Form.Item required label='Username' name='username'>
                                    <Input name='username' />
                                </Form.Item>
                                <Form.Item required label='Password' name='password'>
                                    <Input.Password name='password' />
                                </Form.Item>
                                <Form.Item name='submit' {...buttonItemLayout}>
                                    <SubmitButton>Send</SubmitButton>
                                </Form.Item>
                                <Form.Item name='link' {...buttonItemLayout}>
                                    <Link to='/sign-up'>Sign Up</Link>
                                </Form.Item>
                            </Form>
                        )}
                    />
                </Col>
            </Row>
        </>
    )
}
