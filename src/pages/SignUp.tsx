import { Form, Input, SubmitButton, ResetButton, Select, DatePicker } from 'formik-antd'
import React from 'react'
import { Formik } from 'formik'
import { Col, Row, Typography } from 'antd'
import { createUser } from '../api/auth'
import moment from 'moment'
import * as Yup from 'yup'
import 'yup-phone'
import { Link } from 'react-router-dom'

const { Title } = Typography
const { Option } = Select

const SelectOptions = ['admin', 'god', 'your mom']

const formItemLayout = {
    labelCol: { xs: { span: 24 }, sm: { span: 5 } },
    wrapperCol: { xs: { span: 24 }, sm: { span: 14 } },
}

const buttonItemLayout = { wrapperCol: { sm: { span: 14, offset: 5 }, xs: { span: 24, offset: 0 } } }

const SignUpSchema = Yup.object().shape({
    email: Yup.string().email().required(),
    phone: Yup.string().phone().required(),
    fullName: Yup.string().required(),
    password: Yup.string().min(6, 'Min 6 symbols').required(),
    username: Yup.string().required(),
    birthday: Yup.string().required(),
    roles: Yup.array().min(1),
})

export const SignUp = () => {
    return (
        <>
            <div>
                <Title style={{ textAlign: 'center' }}>Sign Up</Title>
            </div>
            <Row justify={'center'}>
                <Col xs={24} md={14} lg={10}>
                    <Formik
                        initialValues={{
                            email: '',
                            phone: '',
                            fullName: '',
                            password: '',
                            username: '',
                            birthday: '',
                            roles: [],
                        }}
                        validationSchema={SignUpSchema}
                        onSubmit={createUser}
                        render={({ isSubmitting }) => (
                            <Form {...formItemLayout}>
                                {/* every formik-antd component must have the 'name' prop set: */}
                                <Form.Item required label='Email' name='email'>
                                    <Input name='email' />
                                </Form.Item>
                                <Form.Item required label='Phone' name='phone'>
                                    <Input name='phone' placeholder='+79995553232' />
                                </Form.Item>
                                <Form.Item required label='Full name' name='fullName'>
                                    <Input name='fullName' placeholder='Vanya Kuznetcov' />
                                </Form.Item>
                                <Form.Item required label='Password' name='password'>
                                    <Input.Password name='password' />
                                </Form.Item>
                                <Form.Item required label='Username' name='username'>
                                    <Input name='username' />
                                </Form.Item>
                                <Form.Item required label='Birthday' name='birthday'>
                                    <DatePicker name='birthday' defaultPickerValue={moment('1998-12')} />
                                </Form.Item>
                                <Form.Item required label='Roles' name='roles'>
                                    <Select
                                        style={{ width: '100%' }}
                                        name='roles'
                                        mode='multiple'
                                        allowClear
                                        placeholder='Please select'
                                    >
                                        {SelectOptions.map((el, idx) => (
                                            <Option key={idx} value={el}>
                                                {el}
                                            </Option>
                                        ))}
                                    </Select>
                                </Form.Item>
                                <Form.Item name='submit' {...buttonItemLayout}>
                                    <SubmitButton>Send</SubmitButton>
                                </Form.Item>
                                <Form.Item name='link' {...buttonItemLayout}>
                                    <Link to='/sign-in'>Sign In</Link>
                                </Form.Item>
                            </Form>
                        )}
                    />
                </Col>
            </Row>
        </>
    )
}
