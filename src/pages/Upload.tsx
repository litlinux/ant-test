import React, {useState} from 'react'
import {Button, Col, Row, Upload} from 'antd'
import {PlusOutlined} from '@ant-design/icons';
import {toast} from "react-toastify";
import {UploadOutlined} from '@ant-design/icons';
import axios from "../utils/axios";
import moment from "moment";
import Cookies from "universal-cookie";
import {Formik} from "formik";
import {getTokens} from "../api/auth";
import {Form, Input, SubmitButton} from "formik-antd";
import * as Yup from "yup";
import {isEqual} from "lodash";
import {Simulate} from "react-dom/test-utils";

const formItemLayout = {
    labelCol: { xs: { span: 24 }, sm: { span: 5 } },
    wrapperCol: { xs: { span: 24 }, sm: { span: 14 } },
}
const buttonItemLayout = { wrapperCol: { sm: { span: 14, offset: 5 }, xs: { span: 24, offset: 0 } } }

const ImageSchema = Yup.object().shape({
    title: Yup.string().required(),
    desc: Yup.string().required(),
})

const UploadImg = (values: any) => {
    const [fileList, setFileList] = useState<any>([])
    const [file, setFile] = useState<any>()
    const uploadReq = async (values: any) => {
        if (fileList.length < 1) return toast.error('Image is required')
        const formData = new FormData()

        formData.set('file', file)

        console.log(formData)
        try {
            const create = await axios.post('/api/media_objects', formData)
            console.log(create)
            await axios.post('/api/photos', {
                name: values.title,
                dateCreate: new Date(),
                description: values.desc,
                new: true,
                popular: true,
                image: create.config.url + '/' + create.data.id
            })
            toast.success('Successfully uploaded')
        } catch (err) {
            toast.error('Something went wrong')
        }
    }


    return (
        <Row justify='center'>
            <Col xs={24} md={14} lg={10}>
                <Formik
                    initialValues={{
                        title: '',
                        desc: '',
                    }}
                    onSubmit={uploadReq}
                    validationSchema={ImageSchema}
                    render={() => (
                        <Form {...formItemLayout}>
                            <Form.Item required label='Title' name='title'>
                                <Input name='title' />
                            </Form.Item>
                            <Form.Item required label='Description' name='desc'>
                                <Input name='desc' />
                            </Form.Item>
                            <Form.Item label='Image' name='image'>
                                <Upload
                                    name='image'
                                    maxCount={1}
                                    beforeUpload={() => false}
                                    listType='picture'
                                    onChange={file => {
                                        setFile(file.file)
                                        setFileList(file.fileList)
                                    }}
                                >
                                    <Button icon={<UploadOutlined/>}>Choose file</Button>
                                </Upload>
                            </Form.Item>

                            <Form.Item name='submit' {...buttonItemLayout}>
                                <SubmitButton>Send</SubmitButton>
                            </Form.Item>
                        </Form>
                    )}
                />

            </Col>
        </Row>
    )
}

export default UploadImg