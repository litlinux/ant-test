import { Route, Redirect } from 'react-router-dom'
import { useSelector } from 'react-redux'

const PrivateRoute = ({ component: Component, ...rest }: any) => {
    const state = useSelector<any, any>((state) => state.auth)
    return (
        <Route
            {...rest}
            render={(props: any) =>
                state.isAuth ? (
                    <Component {...props} />
                ) : (
                    <Redirect to={{ pathname: '/sign-in', state: { from: props.location } }} />
                )
            }
        />
    )
}
export default PrivateRoute
