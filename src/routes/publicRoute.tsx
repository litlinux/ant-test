import { Route, Redirect } from 'react-router-dom'
import { useSelector } from 'react-redux'

const PublicRoute = ({ component: Component, ...rest }: any) => {
    const state = useSelector<any, any>((state) => state.auth)
    return (
        <Route
            {...rest}
            render={(props: any) =>
                state.isAuth ? (
                    <Redirect to={{ pathname: '/', state: { from: props.location } }} />
                ) : (
                    <Component {...props} />
                )
            }
        />
    )
}

export default PublicRoute
