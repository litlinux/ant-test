import { AUTH_LOGIN, AUTH_LOGOUT } from '../constants'

export const logout = () => async (dispatch: any) => {
    dispatch({
        type: AUTH_LOGOUT,
    })
}

export const login = () => async (dispatch: any) => {
    dispatch({
        type: AUTH_LOGIN,
    })
}
