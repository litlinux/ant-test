import { createStore, applyMiddleware, Store as ReduxStore } from 'redux'
import thunk from 'redux-thunk'
import { persistReducer, persistStore } from 'redux-persist'
import storage from 'redux-persist/lib/storage'

import { reducers, initialState } from './reducers'

const persistConfig = {
    key: 'app',
    storage,
}
const persistedReducer = persistReducer(persistConfig, reducers)

export type Store = ReduxStore<typeof initialState>

const store = () => {
    const store: Store = createStore(persistedReducer, applyMiddleware(thunk))
    const persistor = persistStore(store)
    return { store, persistor }
}

export default store
